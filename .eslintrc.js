module.exports = {
  root: true,
  env: {
    node: true
  },
  globals: {
    Stats: true,
    DS: true,
    clay: true,
  },
  'extends': [
    'plugin:vue/essential',
    'eslint:recommended'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // 'indent': ['error', 2],
    // 'prettier/prettier': 'warn',
    'vue/html-closing-bracket-spacing': ['error', {
      'selfClosingTag': 'always',
    }],
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
}
