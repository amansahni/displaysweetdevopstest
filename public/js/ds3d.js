"use strict";

function DS(app)
{
    this._current_page = "building"; // building/level/floor

    this._camera = null;
    this._control = null;
    this._app = app;

    this._apartments = [];
    this._amenities = [];

    this._picking = null;
    this._onPickedCallback = function() {};
    this._on3DTransitionCallback = function(before, after) {};
    this._onBuildingLoadedCallback = function() {};
    this._highlighted_apartments = [];
    this._highlighted_amenities = [];
    this._timeline = new clay.Timeline();

    this._main_building_node = null;
    this._apartment_block_node = null;
    this._level_plan_node = null;

    this.init(app);

    this._timeline.start();
    DS.ds = this;
    console.log("DS created");
}

DS.prototype.ds = null;

DS.prototype.init = function (app) {

    // Create camera
    this._camera = app.createCamera([-10, 12, -15], [0, 4, 1.5]);
    this._camera.name = "Main Camera";

    // Use orbit control
    this._control = new clay.plugin.OrbitControl({
        target: this._camera,
        domElement: app.container,
        maxAlpha: 90,
        minAlpha: 0.5,
        maxDistance: 30,
        minDistance: 5,
    });
    this._control.name = "Orbit Control";
    //this._control.panMouseButton = '';

    this.preapreMaterials();
    this.setUp3DPicking(app);

    this.loadBuildings(app);
    this.createGround(app);
    //this.loadTrees();
    //this.createPBRSpheres(app);

    /*
    // Sky texture
    // http://www.hdri-hub.com/hdrishop/freesamples/freehdri/item/113-hdr-111-parking-space-free
    var texture = new clay.Texture2D({
        flipY : false
    });
    texture.load('assets/textures/sky_map_rs.png');

    var skydome = new clay.plugin.Skydome({
        scene: app.scene
    });
    skydome.setEnvironmentMap(texture);
    */
    // Set up lightings
    app.createAmbientCubemapLight('./assets/textures/hdr/hall.hdr', 0.7, 0.5, 1);
    app.createDirectionalLight([-1, -1, -1], '#fee', 1);
    //app.createAmbientLight('#fff', 0.3);

    app.scene.name = 'Scene 01';
};

DS.prototype.getCurrentState = function() {
    return this._current_page;
};

DS.prototype.preapreMaterials = function() {
    this._material_apartments = new clay.Material({
        shader: clay.shader.library.get('clay.standard')
    });

    this._material_apartments.set('color', [24/255, 93/255, 76/255]);
    this._material_apartments.set('emission', [24/255, 93/255, 76/255]);
    this._material_apartments.set('alpha', 0.7);
    this._material_apartments.transparent = true;
    this._material_apartments.depthMask = false;

    this._material_desaturated = new clay.Material({
        shader: clay.shader.library.get('clay.standard')
    });

    this._material_desaturated.set('color', [24/255, 93/255, 76/255]);
    this._material_desaturated.set('emission', [24/255, 93/255, 76/255]);
    this._material_desaturated.set('alpha', 0.7);
    this._material_desaturated.transparent = true;
    this._material_desaturated.depthMask = false;

    this._material_amenities = new clay.Material({
        shader: clay.shader.library.get('clay.standard')
    });
    this._material_amenities.set('color', [1, 0, 0]);
    this._material_amenities.set('alpha', 0.7);
    this._material_amenities.transparent = true;
    this._material_amenities.depthMask = false;

    this._material_picking = new clay.Material({
        shader: clay.shader.library.get('clay.standard')
    });
    this._material_picking.set('color', [0/255, 0/255, 0/255]);
    this._material_picking.set('emission', [255/255, 96/255, 0/255]);
    this._material_picking.set('alpha', 0.75);
    this._material_picking.transparent = true;
    this._material_picking.depthMask = false;

    this._material_hidden = new clay.Material({
        shader: clay.shader.library.get('clay.standard')
    });
    this._material_hidden.set('color', [1, 1, 1]);
    this._material_hidden.set('alpha', 0.0);
    this._material_hidden.transparent = true;
    this._material_hidden.depthMask = false;
}

DS.prototype.loadBuildings = function(app) {

    let self = this;

    app.loadModel('./assets/level_plan/level_plan.glb', {
        waitTextureLoaded: true
    }).then(function(result) {            
        self._level_plan_node = result.rootNode;

        self._level_plan_node.scale.set(0.1, 0.1, 0.1);
        self._level_plan_node.position.set(0, 0, 0);
        self._level_plan_node.name = "Level Plan";
        self.disablePicking(self._level_plan_node);
        self._level_plan_node.getDescendantByName("Ground_01").ignorePicking = false;
    });

    app.loadModel('./assets/dest/building.gltf', {
        waitTextureLoaded: false
    }).then(function(result) {
        result.rootNode.scale.set(0.1, 0.1, 0.1);
        result.rootNode.name = "Main Building";
        self.disablePicking(result.rootNode);
        self._main_building_node = result.rootNode;

        //if (self._level_plan_node) {
        //    self._level_plan_node.position.set(0, 0, -10000);
        //}
        self._onBuildingLoadedCallback();
    });
    /*
    app.loadModel('./assets/dest/trees.gltf', {
        waitTextureLoaded: true
    }).then(function(result) {
        result.rootNode.scale.set(0.1, 0.1, 0.1);
        result.rootNode.name = "Trees";
    });
    */
    app.loadModel('./assets/dest/apartment_blocks2.gltf', {})
        .then(function(result) {
            result.rootNode.scale.set(0.1, 0.1, 0.1);
            result.rootNode.name = "Apartment Root";
            //result.rootNode.invisible = true;
            self.prepareApartmentList();
            self.hideAllApartments();

            self.prepareAmenities();
            self.hideAllAmenities();

            //self.highlightApartments(['G10', '201', '205', '1402']);
            //self.highlightAmenities(['childcare', 'gym']);
            self._apartment_block_node = result.rootNode;
        });
};

DS.prototype.createGround = function (app) {

    clay.Shader.import(ground_glsl_src);
    var groundMesh = new clay.Mesh({
        isGround: true,
        material: new clay.Material({
            shader: new clay.Shader({
                vertex: clay.Shader.source('qmv.ground.vertex'),
                fragment: clay.Shader.source('qmv.ground.fragment')
            }),
            transparent: false
        }),
        castShadow: false,
        geometry: new clay.geometry.Plane(),
        renderOrder: -10
    });
    groundMesh.material.set('color', [1,1 ,1, 1]);
    groundMesh.material.set('gridColor', [0.5, 0.5, 0.5, 1]);
    groundMesh.material.set('gridColor2', [0.5, 0.5, 0.5, 1]);
    groundMesh.material.set('showGrid', true);
    groundMesh.scale.set(50, 50, 1);
    groundMesh.rotation.rotateX(-Math.PI / 2);
    this._ground_node = groundMesh;
    this._ground_node.name = "Ground";
    this._app.scene.add(groundMesh);
};

DS.prototype.loadTrees = function() {
    let loader = new clay.loader.GLTF({
        rootNode: new clay.Node()
    });
    loader.load('./assets/trees/trees.gltf');

    let scene = this._app.scene;
    loader.on('success', function (res) {
        scene.add(loader.rootNode);
        loader.rootNode.scale.set(0.1, 0.1, 0.1);
    });
};

DS.prototype.createPBRSpheres = function(app) {
    for (var i = 0; i < 10; i++) {
        app.createSphere({
            metalness: 1,
            roughness: i / 10
        }).position.set((i - 5) * 3, -2, 0);

        app.createSphere({
            metalness: 0,
            roughness: i / 10
        }).position.set((i - 5) * 3, -5, 0);
    }
};

DS.prototype.setUp3DPicking = function(app) {

    let picking = new clay.picking.RayPicking({
        renderer : app.renderer,
        scene: app.scene,
        camera: this._camera
    });

    let self = this;

    // set a time threshold to prevent 3d picking at the end of rotating.
    app.renderer.canvas.addEventListener('mousedown', function(e) {
        self.mouseDownTimeStamp = e.timeStamp;
    });
    app.renderer.canvas.addEventListener('mouseup', function(e) {
        let mouseUpTimeStamp = e.timeStamp;
        if ((mouseUpTimeStamp - self.mouseDownTimeStamp) < 300)
        {
            var res = picking.pick(e.offsetX, e.offsetY);
            if (res) {
                let picked_node = res.target;
                self.sceneNodePicked(picked_node);
            } else {
                self.sceneNodePicked(null);
            }
        }
    });
}

DS.prototype.prepareApartmentList = function() {
    let scene = this._app.scene;
    let apartment_root = scene.getDescendantByName("apartments");

    let level_nodes = [];
    apartment_root.eachChild(function(child) {
        level_nodes.push(child);
    });

    let apartments = [];
    for (let l of level_nodes) {
        l.eachChild(function(child) {
            apartments.push(child);
        });
    }
    this._apartments = apartments;
};

DS.prototype.prepareAmenities = function() {
    let amenitiy_node = this._app.scene.getDescendantByName('amenities');
    //console.log(amenitiy_node);

    let amenities = []
    amenitiy_node.eachChild(function(child) {
        amenities.push(child);
    });
    this._amenities = amenities;
};

DS.prototype.hideAllApartments = function () {
    for (let a of this._apartments) {
        a.material = this._material_hidden;
    }
};

DS.prototype.hideAllAmenities = function() {
    for (let a of this._amenities) {
        a.material = this._material_hidden;
    }
};

DS.prototype.highlightApartments = function(highlighted_apartment_names, color) {

    let c = color || [24/255, 93/255, 76/255, 0.1];

    let mtl = new clay.Material({
        shader: clay.shader.library.get('clay.standard')
    });

    mtl.set('color', [c[0], c[1], c[2]]);
    mtl.set('emission', [c[0], c[1], c[2]]);
    mtl.set('alpha', c[3]);
    mtl.transparent = true;
    mtl.depthMask = false;

    this._highlighted_apartments = [];

    for (let name of highlighted_apartment_names) {
        this._highlighted_apartments.push('apt_'.concat(name));
    }

    this.hideAllApartments();
    this.applyApartmentColoursInternal(this._highlighted_apartments, mtl);
    this._material_apartments = mtl;

    let self = this;
    let obj = {a: 0};
    let animator = this._timeline.animate(obj)
        .when(0, {a: 0})
        .when(250, {a: 0.7})
        .during(function(obj, percent) {
            self._material_apartments.set('alpha', obj.a);
        });
    animator.start('sinusoidalOut');
};

DS.prototype.applyApartmentColoursInternal = function(apartment_list, highlight_material) {
    for (let name of apartment_list) {
        for (let a of this._apartments) {
            if (a.name === name) {
                a.material = highlight_material;
                break;
            }
        }
    }
};

DS.prototype.highlightAmenities = function(highlighted_amenity_names, color) {

    if (highlighted_amenity_names.length == 0) {
        return;
    }
    let c = color || [24/255, 93/255, 76/255, 0];

    let mtl = new clay.Material({
        shader: clay.shader.library.get('clay.standard')
    });

    mtl.set('color', [c[0], c[1], c[2]]);
    mtl.set('emission', [c[0], c[1], c[2]]);
    mtl.set('alpha', c[3]);
    mtl.transparent = (c[3] !== 1.0);

    this._highlighted_amenities = [];

    for (let highlight_name of highlighted_amenity_names) {
        for (let a of this._amenities) {
            if (a.name === highlight_name) {
                this._highlighted_amenities.push(a.name);
            }
        }
    }
    this._material_amenities = mtl;
    this.applyAmenityColoursInternal(this._highlighted_amenities, this._material_amenities);

    let self = this;
    let obj = {a: 0};
    let animator = this._timeline.animate(obj)
        .when(0, {a: 0})
        .when(250, {a: 0.7})
        .during(function(obj, percent) {
            self._material_amenities.set('alpha', obj.a);
        });
    animator.start('sinusoidalOut');
};

DS.prototype.applyAmenityColoursInternal = function(amenity_list, highlight_material) {
    for (let name of amenity_list) {
        for (let a of this._amenities) {
            if (a.name === name) {
                a.material = highlight_material;
                break;
            }
        }
    }
};

DS.prototype.resetHighlighting = function() {
    this.hideAllAmenities();
    this.hideAllApartments();
    this._highlighted_apartments = [];
    this._highlighted_amenities = [];
};

DS.prototype.disablePicking = function(node) {
    node.ignorePicking = true;
    for (let child_node of node._children) {
        child_node.ignorePicking = true;
        this.disablePicking(child_node);
    }
};

DS.prototype.sceneNodePicked = function(node) {

    if (this._current_page === 'level') {
        if (node && node.name === 'Ground_01') {
            this.flyIn();
            this._on3DTransitionCallback('level', 'floor');
        }
    } /*else if (this._current_page === 'floor') {
        let needToFlyOut = (node === null) || (node.material.get('alpha') === 0);
        if (needToFlyOut) {
            this.flyOut();
            this._on3DTransitionCallback('floor', 'level');
        }
    } */else {
        if (node && node.name.startsWith('apt_')) {
            let apartment_name = node.name.replace('apt_', '');
            console.log('Picked an apartment:', apartment_name);

            this.hideAllApartments();
            this.hideAllAmenities();
            this.applyApartmentColoursInternal(this._highlighted_apartments, this._material_apartments);
            this.highlightPickedNode(node);

            this.onPicked(this._onPickedCallback(apartment_name));
            return;
        }
        if (node && ['childcare', 'gym', 'pool', 'communal', 'retail'].includes(node.name)) {
            console.log('Picked an amenity: ', node.name);

            this.hideAllApartments();
            this.hideAllAmenities();
            this.applyApartmentColoursInternal(this._highlighted_apartments, this._material_apartments);
            this.applyAmenityColoursInternal(this._highlighted_amenities, this._material_amenities);
            this.highlightPickedNode(node);
            this.onPicked(this._onPickedCallback(node.name));
            return;
        }
    }
    //console.log("Picked a scene node:", node.name);
};

DS.prototype.highlightPickedNode = function(node) {
    //console.log(node);
    node.material = this._material_picking;
    let self = this;
    let obj = {a: 1};
    let animator = this._timeline.animate(obj)
        .when(0, {a: 1})
        .when(250, {a: 0.75})
        .during(function(obj) {
            self._material_picking.set('alpha', obj.a);
        });
    animator.start('sinusoidalOut');
};

/**
 * @param {function} callback the callback function when an apartment/amenity is picked in the 3d view
 */
DS.prototype.onPicked = function(callback) {

    if (typeof callback === 'function') {
        this._onPickedCallback = callback;
        return true;
    } else {
        console.log(callback, "is not a function!");
        return false;
    }
};

DS.prototype.on3DTransition = function(callback) {
    if (typeof callback === 'function') {
        this._on3DTransitionCallback = callback;
        return true;
    } else {
        console.log(callback, "is not a function!");
        return false;
    }
};

DS.prototype.onBuildingLoaded = function(callback) {
    if (typeof callback === 'function') {
        this._onBuildingLoadedCallback = callback;
        return true;
    } else {
        this._onBuildingLoadedCallback = function() {};
        return false;
    }
};

DS.prototype.flyIn = function() {
    if (this._current_page === 'building') {
        this.buildingToLevel();
        this._current_page = 'level';
    } else if (this._current_page === 'level') {
        this.levelToFloor();
        this._current_page = 'floor';
    }
    this.setZoomSensibility();
    return this._current_page;
};

DS.prototype.flyOut = function() {
    if (this._current_page === 'level') {
        this.levelToBuilding();
        this._current_page = 'building';
    } else if (this._current_page === 'floor') {
        this.floorToLevel();
        this._current_page = 'level';
    }
    this.setZoomSensibility();
    return this._current_page;
};

DS.prototype.buildingToLevel = function() {

    let self = this;
    let camera_focus_y = this._control.getCenter()[1];
    this._level_plan_node.position.set(0, camera_focus_y, 0.8);

    this._ground_node.invisible = true;

    // normalise beta
    this.normaliseBetaAngle(this._control);

    this._control.minDistance = 3;
    this._control.maxDistance = 10;

    this._control.timeline = this._timeline;
    this._control.animateTo({
        distance: 7,
        duration: 1000,
        alpha: 60,
        beta: 0,
        easing:'cubicOut',
        done: function() {}
    });

    this._animated_materials = [];
    let animated_materials = this._animated_materials;
    if (animated_materials.length === 0) {
        this.collectMaterials(this._main_building_node, animated_materials);
    }

    this.original_alphas = {};
    for (let m of animated_materials) {
        this.original_alphas[m.name] = m.get('alpha');
        m.transparent = true;
    }

    self._material_apartments.depthMask = true;
    self._material_picking.depthMask = true;

    let obj = {a: 1};
    let animator = this._timeline.animate(obj)
        .when(0, {a: 1})
        .when(400, {a: 0})
        .during(function(obj, percent) {
            /*
            for (let m of animated_materials) {
                m.set('alpha', obj.a);
            }

            self._material_apartments.set('alpha', obj.a * 0.75);
            self._material_picking.set('alpha', obj.a * 0.75);
            */

        }).done(function() {
            /*
            for (let m of animated_materials) {
                m.depthMask = false;
            }
            */
            self._apartment_block_node.position.set(0, 0, -10000);
            self._main_building_node.position.set(0, 0, -10000);
        });
    animator.start('linear');
};

DS.prototype.levelToBuilding = function() {
    let self = this;

    this._ground_node.invisible = false;

    // normalise beta
    this.normaliseBetaAngle(this._control);

    this._control.maxDistance = 30;
    this._control.timeline = this._timeline;
    this._control.animateTo({
        distance: 20,
        duration: 1000,
        alpha: 20,
        beta: 140,
        easing:'cubicOut',
        done: function() {
            self._control.minDistance = 5;
        }
    });

    let animated_materials = this._animated_materials;
    for (let m of animated_materials) {
        m.depthMask = true;
    }

    let obj = {a: 0};
    let animator = this._timeline.animate(obj)
        .when(0, {a: 0})
        .when(400, {a: 1})
        .during(function(obj, percent) {
            /*
            for (let m of animated_materials) {
                let alpha = self.original_alphas[m.name] * percent;
                m.set('alpha', alpha);
            }
            */

        }).done(function() {
            for (let m of animated_materials) {
                let alpha = self.original_alphas[m.name];
                m.set('alpha', alpha);
            }
            for (let m of animated_materials) {
                if (m.get('alpha') === 1) {
                    m.trasparent = false;
                    m.depthMask = true;
                } else {
                    m.trasparent = true;
                    m.depthMask = false;
                }
            }

            self._material_apartments.set('alpha', 0.75);
            self._material_picking.set('alpha', 0.75);
            self._material_apartments.depthMask = false;
            self._material_picking.depthMask = false;

            //self.resetHighlighting();
            self._apartment_block_node.position.set(0, 0, 0);
            self._main_building_node.position.set(0, 0, 0);
            self._level_plan_node.position.set(0, 4, -10000);
        });
    animator.start('linear');
};

DS.prototype.levelToFloor = function() {
    let self = this;
    let pos = this._level_plan_node.position.clone();
    let obj = {x: pos[0], z: pos[2]};
    let y = pos.array[1];

    let animator = this._timeline.animate(obj)
        .when(0, {x: 0, z: 0.8})
        .when(500, {x: -0.6, z: 0.3})
        .during(function(prop, percent) {
            //console.log(prop);
            self._level_plan_node.position.set(prop.x, y, prop.z);

        }).done(function() {});

    animator.start('cubicOut');

    this.setFloorPlanAlpha(1, 0);

    this._control.minDistance = 1;
    this._control.timeline = this._timeline;
    this._control.animateTo({
        distance: 2,
        duration: 500,
        alpha: 60,
        easing:'cubicOut',
        done: function() {
            self._control.maxDistance = 5;
        }
    });
};

DS.prototype.floorToLevel = function() {
    let self = this;
    let pos = this._level_plan_node.position.clone();

    let obj = {x: pos[0], z: pos[2]};
    let animator = this._timeline.animate(obj)
        .when(0, {x: -0.6, z: 0.3})
        .when(500, {x: 0, z: 0.8})
        .during(function(prop, percent) {
            let y = pos.array[1];
            self._level_plan_node.position.set(prop.x, y, prop.z);
        }).done(function() {});
    animator.start('cubicOut');

    this.setFloorPlanAlpha(0, 1);

    this._control.maxDistance = 10;
    this._control.animateTo({
        distance: 7,
        duration: 500,
        easing:'cubicOut',
        done: function() {
            self._control.minDistance = 3;
        }
    });
};

DS.prototype.collectMaterials = function(node, materials) {
    if (node) {
        if (node.material) {
            materials.push(node.material);
        }
        for (let child_node of node._children) {
            this.collectMaterials(child_node, materials);
        }
    }
};

DS.prototype.setCameraHeight = function(y) {
    const current_control_position = this._control.getCenter();
    this._control.timeline = this._timeline;
    this._control.animateTo({
        duration: 50,
        center: [current_control_position[0], y, current_control_position[2]],
        // easing: 'cubicOut',
    });
};

DS.prototype.setWallsVisible = function(b) {
    if (typeof b !== 'boolean')
        console.log("It's not a boolean!");

    this._level_plan_node.getDescendantByName("polySurface46").invisible = !b;
};

DS.prototype.setOutsideWallsVisible = function(b) {
    this._level_plan_node.getDescendantByName("OutsideWalls_01").invisible = !b;
};

DS.prototype.setGroundVisible = function(b) {
    this._level_plan_node.getDescendantByName("Ground_01").invisible = !b;
};

DS.prototype.setFloorPlanAlpha = function(alphaBegin, alphaEnd) {

    let materials = [];
    let initialAlphas = [];
    materials.push(this._level_plan_node.getDescendantByName("polySurface46").material);
    initialAlphas.push(1);
    materials.push(this._level_plan_node.getDescendantByName("OutsideWalls_01").material);
    initialAlphas.push(0.2);
    materials.push(this._level_plan_node.getDescendantByName("Ground_01").material);
    initialAlphas.push(1);

    let self = this;
    let obj = {a:1};

    let animator = this._timeline.animate(obj)
    .when(0, {a: alphaBegin})
    .when(500, {a: alphaEnd})
    .during(function(prop, percent) {
        for (let i = 0; i < materials.length; i += 1) {
            materials[i].set('alpha', prop.a * initialAlphas[i]);
        }
    }).done(function() {

        for (let m of materials) {
            if (m.get('alpha') === 1) {
                m.transparent = false;
                m.depthMask = true;
            } else {
                m.transparent = true;
                m.depthMask = false;
            }
        }
    });

    for (let m of materials) {
        m.transparent = true;
        m.depthMask = false;
    }
    animator.start('cubicOut');
};

DS.prototype.showLevelPlanOnly = function() {
    this.setVisibilityRecursively(this._level_plan_node, false);

    this.setWallsVisible(true);
    this.setOutsideWallsVisible(true);
    this.setGroundVisible(true);
};

DS.prototype.setVisibilityRecursively = function(node, visible) {
    if (node) {
        if (node.geometry) {
            node.invisible = !visible;
        }
        for (let child_node of node._children) {
            this.hideAllExcept(child_node, visible);
        }
    }
};

DS.prototype.setZoomSensibility = function() {
    if (this._current_page === 'building') {
        this._control.zoomSensitivity = 1;
    } else if (this._current_page === 'level') {
        this._control.zoomSensitivity = 0.3;
    } else if (this._current_page === 'floor') {
        this._control.zoomSensitivity = 0.1;
    }
};

DS.prototype.normaliseBetaAngle = function(orbitControl) {
    let beta = orbitControl.getBeta();
    beta = beta % 360;
    if (beta < 0) beta += 360;
    if (beta > 360) beta -= 360;
    orbitControl.setBeta(beta);
};