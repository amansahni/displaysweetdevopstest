FROM nginx
MAINTAINER Aren Villanueva <aren@displaysweet.com>
COPY dist.tar dist.tar
RUN ls -lah
RUN tar xvf dist.tar -C /usr/share/nginx/html
COPY .nginx-default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
EXPOSE 81
WORKDIR /etc/nginx
CMD ["nginx", "-g", "daemon off;"]