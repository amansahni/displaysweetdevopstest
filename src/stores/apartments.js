import apartments from './destination-apartments';

const state = {
  collection: apartments.listings,
};

const getters = {
  getById: state => id => state.collection.find(project => project.id === id),
  getByName: state => name => state.collection.find(project => project.name === name),
};

const mutations = {};

const actions = {};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
