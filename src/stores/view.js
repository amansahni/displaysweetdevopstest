
const state = {
  selectedApartmentName: null,
  selectedDimension: '3d',
  selectedLevel: 6,
  loading: false,
};

const getters = {
};

const mutations = {
  SET_APARTMENT(state, apartmentName) {
    state.selectedApartmentName = apartmentName;
  },
  SET_LEVEL(state, level) {
    state.selectedLevel = level;
  },
  SET_DIMENSION(state, dimension) {
    state.selectedDimension = dimension;
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
};

const actions = {
  setApartment({ commit }, apartmentName) {
    commit('SET_APARTMENT', apartmentName);
  },
  setLevel({ commit }, level) {
    commit('SET_LEVEL', level);
  },
  setDimension({ commit }, dimension) {
    commit('SET_DIMENSION', dimension);
  },
  setLoading({ commit }, loading) {
    commit('SET_LOADING', loading);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
