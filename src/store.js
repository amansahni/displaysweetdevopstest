import Vue from 'vue'
import Vuex from 'vuex'

import view from './stores/view';
import apartments from './stores/apartments';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    view,
    apartments,
  }
})
