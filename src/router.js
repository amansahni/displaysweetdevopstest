import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

// TODO change this back to home to view app
import Splash from './views/Splash.vue'

const urlHasTrigger = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const tParam = urlParams.get('divdiv');
  return tParam !== null;
};

const Component = urlHasTrigger() ? Home : Splash;

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      // name: 'home',
      // component: Home,
      // TODO change this back to home to view app
      component: Component,
      children: [
        {
          path: '/',
          name: 'home',
          components: {
            default: () => import(/* webpackChunkName: "loading" */ './views/Loading.vue'),
            sidebar: () => import(/* webpackChunkName: "mainSidebar" */ './views/sidebars/MainSidebar.vue'),
          }
        },
        {
          path: '/level',
          name: 'level',
          components: {
            default: () => import(/* webpackChunkName: "level" */ './views/Level.vue'),
            sidebar: () => import(/* webpackChunkName: "levelSidebar" */ './views/sidebars/LevelSidebar.vue'),
          }
        },
        {
          path: '/floor/:id',
          name: 'floor',
          components: {
            default: () => import(/* webpackChunkName: "floor" */ './views/Floor.vue'),
            sidebar: () => import(/* webpackChunkName: "floorSidebar" */ './views/sidebars/FloorSidebar.vue'),
          }
        },
      ]
    },
  ]
})
